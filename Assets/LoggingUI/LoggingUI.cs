﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class LoggingUI : MonoBehaviour {

    public bool printStackTrace = false;
    public Text logEntryPrefab;

    private GameObject content;

    // Use this for initialization
    void Start()
    {
        Application.logMessageReceived += LogMessageHandler;
        RectTransform contRect = GetComponent<ScrollRect>().content;
        content = contRect.gameObject;

    }

    private void LogMessageHandler(string logMessage, string stackTrace, LogType type)
    {
        string toPrint = logMessage;
        if (printStackTrace){
            toPrint = toPrint + "\n" + stackTrace;
        }
        switch (type){
            case LogType.Warning:
                toPrint = "Warning: " + toPrint;
                break;
            case LogType.Error:
                toPrint = "Error: " + toPrint;
                break;
            default:
                break;
        }

        Text nextLog = Instantiate<Text>(logEntryPrefab, content.transform);
        nextLog.text = toPrint;
    }
}
