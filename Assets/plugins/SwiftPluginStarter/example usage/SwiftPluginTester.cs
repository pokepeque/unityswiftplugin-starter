﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwiftPluginTester : MonoBehaviour {

    public InputField inputField;
    public Text callbackResponse;
    	
    //Tests a simple call to a Swift function
    public void PluginHelloWorld(){

        //Calls HelloWorld in SwiftPluginUnity
        SwiftPluginUnity.HelloWorld();
    }

    public void PluginHelloWho(){
        SwiftPluginUnity.Hello(inputField.text);
    }

    public void PluginHelloSwift()
    {
        SwiftPluginUnity.TriggerCallback();
    }

    public void PluginCallback(string s){
        callbackResponse.text = s;
        StartCoroutine(EmptyTextField(callbackResponse, 3f));
    }

    IEnumerator EmptyTextField(Text field, float delay){
        yield return new WaitForSeconds(delay);
        field.text = "";
    }
}
