//
//  SwiftPluginUnity-Bridging-Header.h
//  SwiftPlugin
//
//  Created by Stefan  on 12.11.18.
//  Copyright © 2018 waku waku. All rights reserved.
//

#ifndef SwiftPluginUnity_Bridging_Header_h
#define SwiftPluginUnity_Bridging_Header_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UnityInterface.h"

#endif /* SwiftPluginUnity_Bridging_Header_h */
