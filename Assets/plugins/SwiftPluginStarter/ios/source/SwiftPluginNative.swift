import Foundation
import UIKit


@objc public class SwiftPluginNative: NSObject {
    
    //Create a static reference to a Swift object, which encapsulates all
    @objc static let shared = SwiftPluginNative()
    
    //CHANGE-HERE: The CallbackTarget is the name GameObject with the plugin component (i.e.
    // the game object on which the unity counterpart - the c#-script - resides)
    // Attention: Not the name of the C# script.
    let kCallbackTarget = "SwiftPluginUnity"
    
    //Declare here all functions
    @objc func HelloWorld() {
        UnityLogger.Log(message: "Swift Plugin says 'Hello World'")
    }
    
    @objc func Hello(_ who: String){
        UnityLogger.Log(message: "Swift Plugin: Hello \(who)");
    }
    
    @objc func TriggerCallback() {
        UnityLogger.Log(message: "Swift Plugin: Unity requested a callback.")
        UnitySendMessage(self.kCallbackTarget, "OnPluginCallbackArgument", "Hello Yuuuunity")
    }
}
