//INSTRUCTIONS ON HOW TO MODIFY THIS FILE:
//Name this file as <NameOfPlugin>Bridge.mm.
//It exposes the plugin's native functions to C#, and forwards
//all function calls to Swift. This file is written in Objective-C.

// Import any libraries if needed.
// #import <library/examplefile.h>

// The following -Swift.h file is very IMPORTANT. Its naming convention is
// "<NameOfModule>-Swift.h", where NameOfModule is the same as the name used in
// "<NameOfModule>-Bridging-Header.h". This file does NOT really exist anywhere
// in the project, it is only generated at build according to this naming convention.
#include "SwiftPluginUnity-Swift.h"


#pragma mark - C interface

extern "C" {

    //Declare all functions here with the name you use in the C#-class to access them.
    void _rp_HelloWorld() {
       [[SwiftPluginNative shared] HelloWorld];
    }
    
    void _rp_Hello(char* helloWho){
        printf("Bridge File received argument: %s\n", helloWho);
        NSString *s = [[NSString alloc] initWithCString:helloWho encoding:NSUTF8StringEncoding];
        [[SwiftPluginNative shared] Hello:s ];
    }
    
    void _rp_TriggerCallback(){
        [[SwiftPluginNative shared] TriggerCallback];
    }
}
