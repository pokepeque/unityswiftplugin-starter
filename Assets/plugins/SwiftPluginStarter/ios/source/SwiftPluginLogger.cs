﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwiftPluginLogger : MonoBehaviour
{
    #region Logging Function Implementation
    //Note: Any Log Message is printed using the debug log.
    //Logging there triggers an event on Application.logMessageReceived.
    //Can be rerouted to other location via this event.

    //Function is called from Swift
    public void LogSwiftMessage(string message)
    {
        Debug.Log("SL: " + message);
    }

    //Function is called from Swift
    public void LogSwiftWarning(string message)
    {
        Debug.LogWarning("SL: " + message);
    }

    //Function is called from Swift
    public void LogSwiftError(string message)
    {
        Debug.LogError("SL: " + message);
    }
    #endregion

}
