//
//  UnityLogger.swift
//  Unity-iPhone
//
//  Created by Stefan  on 16.11.18.
//
//  This class should be used to log messages to the console AND unity within the plugin
//  It prints the log-message both to the XCode / Device Log as well as sends it back to unity.

import Foundation

@objc public class UnityLogger: NSObject {

    static let kLoggerGameObject = "SwiftPluginUnity"
    
    @objc static func Log(message: String){
        print(message)
        UnitySendMessage(self.kLoggerGameObject, "LogSwiftMessage", message)
    }
    
    @objc static func LogWarning(message: String){
        print("Warning: \(message)")
        UnitySendMessage(self.kLoggerGameObject, "LogSwiftWarning", message)
    }
    
    @objc static func LogError(message: String){
        print ("Error: \(message)")
        UnitySendMessage(self.kLoggerGameObject, "LogSwiftError", message)
    }
}
