﻿using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.Events;


/// <summary>
/// Swift plugin unity.
/// 
/// This is the implementation of a basic plugin for Swift in Unity. Follow the instructions in the readme to use.
/// 
/// Use simply by calling one of the static functions. The plugin will auto-initialize.
/// </summary>
public class SwiftPluginUnity : MonoBehaviour
{

    #region Declare external C interface    

        #if UNITY_IOS && !UNITY_EDITOR
        [DllImport("__Internal")]
        private static extern void _rp_HelloWorld();

        [DllImport("__Internal")]
        private static extern void _rp_Hello(string who);

        [DllImport("__Internal")]
        private static extern void _rp_TriggerCallback();
        #endif

    #endregion

    #region Wrapped methods and properties
    public static void HelloWorld()
    {
        #if UNITY_IOS && !UNITY_EDITOR
            //Calls _rp_HelloWorld declared in the external function call above, which references the function in SwiftPluginBridge.mm
            _rp_HelloWorld();
        #else
            Debug.Log("SwiftPlugin: Hello World. Not supported on this platform");
        #endif
    }

    public static void Hello(string who)
    {
        #if UNITY_IOS && !UNITY_EDITOR
        //Calls _rp_Hello(who) declared in the external function call above, which references the function in SwiftPluginBridge.mm
            _rp_Hello(who);
        #else
        Debug.Log("SwiftPlugin: Hello(who). Not supported on this platform");
        #endif
    }

    public static void TriggerCallback()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        //Calls _rp_TriggerCallback() declared in the external function call above, which references the function in SwiftPluginBridge.mm
        _rp_TriggerCallback();
        #else
        Debug.Log("SwiftPlugin: Hello TriggerCallback. Not supported on this platform");
        #endif
    }
    #endregion

    #region Singleton implementation
    private static SwiftPluginUnity _instance;

    public static SwiftPluginUnity Instance
    {
        get
        {
            if (_instance == null)
            {
                var obj = new GameObject("SwiftPluginUnity");
                _instance = obj.AddComponent<SwiftPluginUnity>();
                obj.AddComponent<SwiftPluginLogger>();
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Delegates
    public UnityEvent onPluginCallback;
    public SwiftPluginArgumentCallback onPluginArgumentCallback;

    public void OnPluginCallback()
    {
        Debug.Log("SwiftPluginUnity: OnPluginCallback was called");
        if (onPluginCallback != null)
        {
            onPluginCallback.Invoke();
        }
    }

    public void OnPluginCallbackArgument(string argument){

        Debug.Log("SwiftPluginUnity: OnPluginCallbackArgument was called with '" + argument + "'");
        if (onPluginArgumentCallback != null){
            onPluginArgumentCallback.Invoke(argument);
        }
    }

    //Event with one argument. Needs to be declared to be assignable.
    [System.Serializable]
    public class SwiftPluginArgumentCallback : UnityEvent<string> { }
    #endregion



}