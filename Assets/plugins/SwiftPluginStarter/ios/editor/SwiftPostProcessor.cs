﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

using System.IO;
using System.Linq;

public static class SwiftPostProcessor {
       [PostProcessBuild]
       public static void OnPostProcessBuild(BuildTarget buildTarget, string buildPath) {
           if(buildTarget == BuildTarget.iOS) {
               // We need to construct our own PBX project path that corrently refers to the Bridging header

                // var projPath = PBXProject.GetPBXProjectPath(buildPath);
               var projPath = buildPath + "/Unity-iPhone.xcodeproj/project.pbxproj";
               var proj = new PBXProject();
               proj.ReadFromFile(projPath);

               var targetGuid = proj.TargetGuidByName(PBXProject.GetUnityTargetName());

            //// Configure build settings
               proj.SetBuildProperty(targetGuid, "ENABLE_BITCODE", "NO");

            //CHANGE HERE: Adapt the version number to the required swift version number
               proj.SetBuildProperty(targetGuid, "SWIFT_VERSION", "4.2");

            //CHANGE HERE: The following file must exist in Unity in the ios folder. Rename it and all lines referring to "SwiftPluginUnity" to the name of your own plugin. 
               //The path is relative to the XCode project that is generated when building in unity. It is a frequent error that XCode cannot find this file when trying to build 
               //the project --> Locate file in the XCode project and add the path here. 
               proj.SetBuildProperty(targetGuid, "SWIFT_OBJC_BRIDGING_HEADER", "Libraries/plugins/SwiftPluginStarter/ios/source/SwiftPluginUnity-Bridging-Header.h");

            //CHANGE HERE: This File must be named according to the Bridging-Header file. Use "NameOfPlugin"-Swift.h.
               proj.SetBuildProperty(targetGuid, "SWIFT_OBJC_INTERFACE_HEADER_NAME", "SwiftPluginUnity-Swift.h");
               proj.AddBuildProperty(targetGuid, "LD_RUNPATH_SEARCH_PATHS", "@executable_path/Frameworks");

               proj.WriteToFile(projPath);
           }
       }
   }